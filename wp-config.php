<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jobs');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm.),XMp%t;_/y>K`i+]o;~#jcA|Cu#Byc4xQ( ;AG(0Se]W6~Om4O2/S:1sblH#g');
define('SECURE_AUTH_KEY',  '$+>,7eI*$rj^!/iMK (5C=W_jM2Qg0BnRh?M}x:rnjd!~NE@gm;W|1%1J_^X3RLh');
define('LOGGED_IN_KEY',    'dDv+i1~HP>VCK>5 +?Tw>sy~=VU4<_XQWmMhk/<~hvln$?HhH~i,9YW9h]S*(Cmc');
define('NONCE_KEY',        'K? -j9Yh|yjl!`$TD@&;WJKzhl`lMOi(Oex/|bx[=t~>+,9H<D =DIq?kjZx#*Or');
define('AUTH_SALT',        'jjZ!Nn<KHAY.kr .IFd*1%/m`0J(:<X}rz:otE{FEq^+U_6^#5}-#~C#7fV~ED%c');
define('SECURE_AUTH_SALT', 'gY}WZ<e9~b55n1q8Es2BPRXjyG3n1Aa[jQ@KVQV:RT`r^@D$F#G5zy+Hj&!z!CRW');
define('LOGGED_IN_SALT',   ';N`9&=FL:+)~SGVVO.7_8_(;-j|a13#yIX$i[{x*9<4{^dl^UjCKND0E&s?{w{&c');
define('NONCE_SALT',       'SM|9DlWF^,{OOfV9G1?>EM:M@qGaDC3C9=Jj(k6y>,AM#w(^}M$EH|ImkW0J$*^~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jobs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
